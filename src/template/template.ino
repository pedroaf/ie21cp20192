#include "Ultrasonic.h" 
const int echoPin = 6; 
const int trigPin = 7; 

const int pinoBuzzer = 11; 

Ultrasonic ultrasonic(trigPin,echoPin); 

int distancia; 

void setup(){
pinMode(echoPin, INPUT); 
pinMode(trigPin, OUTPUT); 
pinMode(pinoBuzzer, OUTPUT);
}
void loop(){

hcsr04();

if(distancia >= 0 && distancia <= 10){
tone(pinoBuzzer,1046*2);
delay(50);
tone(pinoBuzzer,1567*2);
delay(50);

}
else if(distancia >= 11 && distancia <= 21){
tone(pinoBuzzer,523*2);
delay(80);
tone(pinoBuzzer,783*2);
delay(80);

}
else if(distancia >= 22 && distancia <= 32){
tone(pinoBuzzer,261*2);
delay(100);
tone(pinoBuzzer,391*2);
delay(100);

}
else{
noTone(pinoBuzzer);
}
}


void hcsr04(){
digitalWrite(trigPin, LOW); 
delayMicroseconds(2);
digitalWrite(trigPin, HIGH); 
delayMicroseconds(10);
digitalWrite(trigPin, LOW); 

distancia = (ultrasonic.Ranging(CM)); 
delay(0); 
}