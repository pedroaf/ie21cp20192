# Introdução

![](índice.jpg)
Este trabalho consiste em construir um protótipo de sensor de ré.

# Objetivo

Auxiliar em balizas e no estacionamento em garagens.

# Materiais Utilizados

● Arduino UNO com cabo USB A/B.

● Sensor Ultrasônico HC-SR04.

● Bateria de 5V.

● Buzzer ativo 5v.

● Jumpers.

# Esquema Elétrico


![Esquema](https://blogmasterwalkershop.com.br/wp-content/uploads/2016/10/esquem%C3%A1tico-832x1024.png)
![](índice2.jpg)

# Resultados

Conseguimos criar o que seria um protótipo simples que pode ser anexado na parede de uma garagem, porém, para ligar e desligar, é necessário que o compartimento onde o arduino está guardado seja aberto para que se possa conectar a placa do arduino à bateria de 5v, sendo necessário um aprimoramento do mesmo para que seja viável sua utilização.

# Desafios encontrados

Os desafios encontrados foram regular a distância certa de ativação, formas de deixar o som mais alto, tal como remoção de protoboard, de deixar o som mais alto, e remover os delays de aproximação.

# Vídeo de demonstração

{{<youtube "jklE8mL8-lY">}}

# Trabalho de:

● Leonardo Pedrassani

● Pedro Afonso Nogueira dos Santos

● Pedro Bordin