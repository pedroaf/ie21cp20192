# Introdução

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|Pedro Afonso|pedroaf|
|Pedro Bordin|SrBordin|
|Leonardo Pedrassani|lpredrassani|

# Documentação

A documentação do projeto pode ser acessada pelo link:

https://pedroaf.gitlab.io/ie21cp20192/

O vídeo pode ser acessado pelo link no final da página.

# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)